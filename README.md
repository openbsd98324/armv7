# NETBSD armv7 for Raspberry PI


## Hardware ARM Board

http://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/evbarm-earmv7hf/binary/gzimg/armv7.img.gz


Tested on : 

- raspberry pi zero

- raspberry pi rpi3b 

- raspberry pi rpi3b+


> Wireless

**- The wifi works on dongle usb, but the kernel needs to be compiled to get wifi broadcom!**

- So no wifi easily.

> Ethernet RJ45

- Fully working


> USB

- Fully working



> Desktop 

- Fully working (no GPU for fast graphics)



 
![](media/rpi0.png)


![](media/rpi3b+.png)



## Official Image 

www.ftp.netbsd.org

evarm7hf 

1.) wget -c --no-check-certificate   "http://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/evbarm-earmv7hf/binary/gzimg/armv7.img.gz" 


2.) Copy the image onto a SD/MMC card

zcat armv7.img.gz > /dev/rsdX

or use rawrite on windows. https://gitlab.com/openbsd98324/rawrite


3.) The directory pkgsrc will have the packages: 

 
  export PKG_PATH="http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.1/All/" ;   pkg_add -v pkgin ; pkgin update ; pkgin update 




## Devices

All logitech regular Kxxx keyboards will work very well. 

** RECOMMENDED: The K55 Corsair *IS WORKING WELL***
k55 corsair: 
![](media/corsair-k55-netbsd-pi.png)

Note: Only, the G213 Logitech is *NOT* working well.


## Wireless

Wifi Broadcom works very well. To get broadcom wifi, it is needed to recompile the kernel from source.

The usb dongles like Belkin, ... showed as urtwn0, will work very well. 
/etc/wpa_supplicant.conf   <-- wpa_passphrase will add the wpa/key. 


## Configuration

 


> UTF and special chars (Required):

```` 
# NETBSD FILE etc/profile 
export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_ALL=""
export TZ=Europe/Amsterdam
```` 


> Harddisk on pendrive?

```` 
# /boot/cmdline.txt
root=sd0a console=fb
#fb=1280x1024           # to select a mode, otherwise try EDID
#fb=disable             # to disable fb completely
```` 
sd0a will be the pendrive (sanedisk of 32gb) or SSD harddisk. 


> Overscan change (to display, i.e. larger monitor)

```` 
# netbsd 
#/boot/config.txt
disable_overscan=1
```` 



> ALT (optional):  wsconsctl -k -w map+='keycode 56 = Cmd Alt_L'







